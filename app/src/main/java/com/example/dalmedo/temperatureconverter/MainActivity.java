package com.example.dalmedo.temperatureconverter;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText value;
    Button convert;
    RadioGroup radioGroup;
    RadioButton radioButton,radioButton1;
    TextView value2, value3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        value = (EditText) findViewById(R.id.value);
        convert = (Button) findViewById(R.id.converto);
        radioGroup = (RadioGroup) findViewById(R.id.tempType);
        value2 = (TextView) findViewById(R.id.value2);
        value3 = (TextView) findViewById(R.id.value3);
        radioButton =(RadioButton) findViewById(R.id.celsius);
        radioButton1 =(RadioButton) findViewById(R.id.farnheit);

        convert.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String input = value.getText().toString();
                double valueinput = Double.parseDouble(input);

                boolean isvalidinput = isValid(input);
                if (isvalidinput) {
                    int radiobuttonid = radioGroup.getCheckedRadioButtonId();
                    radioButton = (RadioButton) findViewById(radiobuttonid);
                    if (radioButton.getText().toString().equals("Celcius to Farnheit")){

                        double result = ((valueinput*9)/5)+32;
                        value2.setText(String.valueOf(result));
                        value3.setText("Degrees Farnheit");
                    }

                } else {

                    double result = ((valueinput-32)*5)/9;
                    value2.setText(String.valueOf(result));
                    value3.setText("Degrees Celsius");

                    Snackbar.make(v, "PLEASE ENTER A VALID INPUT", Snackbar.LENGTH_LONG).show();

                }
            }
        });

    }

    public boolean isValid(String input) {
        boolean isvalid = true;
        if (input.trim().equals("")) {
            isvalid = false;


        }
        return isvalid;


    }
}